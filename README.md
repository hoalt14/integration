# Jenkins integration with Gitlab, Ansible

- [STEP 1: Install Jenkins Server](#step-1-install-jenkins-server)
  - [Jenkins](#jenkins)
  - [Nginx](#nginx)
- [STEP 2: Install Gitlab Server](#step-2-install-gitlab-server)
- [STEP 3: Config Webapp Server](#step-3-config-webapp-server)
- [STEP 4: Add Credentials for Webapp on Jenkins UI](#step-4-add-credentials-for-webapp-on-jenkins-ui)
- [STEP 5: Add Node on Jenkins UI](#step-5-add-node-on-jenkins-ui)
- [STEP 6: Setting gitlab plugin](#step-6-setting-gitlab-plugin)
  - [Install gitlab plugin on Jenkins UI](#install-gitlab-plugin-on-jenkins-ui)
  - [Add a personal access token on Gitlab UI](#add-a-personal-access-token-on-gitlab-ui)
  - [Add Credentials Gitlab token on Jenkins UI](#add-credentials-gitlab-token-on-jenkins-ui)
  - [Configure Gitlab on Jenkins UI](#configure-gitlab-on-jenkins-ui)
- [STEP 7: Create new user, group, project on Gitlab UI](#step-7-create-new-user-group-project-on-gitlab-ui)
  - [Create new user](#create-new-user)
  - [Create new group](#create-new-group)
  - [Create new project](#create-new-project)
- [STEP 8: Create new Deploy Keys and enable in project on Gitlab UI](#step-8-create-new-deploy-keys-and-enable-in-project-on-gitlab-ui)
  - [Create deploy key](#create-deploy-key)
  - [Enable deploy key](#enable-deploy-key)
- [STEP 9: Setting project on Gitlab UI](#step-9-setting-project-on-gitlab-ui)
  - [Allow push for Developer](#allow-push-for-developer)
  - [Login user developer -> add ssh key -> clone source to local](#login-user-developer-->-add-ssh-key-->-clone-source-to-local)
- [STEP 10: Setting Pipeline, Webhook](#step-10-setting-pipeline-webhook)
  - [Create Pipeline on Jenkins UI](#create-pipeline-on-jenkins-ui)
  - [Create Webhook on Gitlab UI (usage root user)](#create-webhook-on-gitlab-ui-(usage-root-user))
  - [Define pipeline script on Jenkins UI](#define-pipeline-script-on-jenkins-ui)
- [STEP 11: Testing](#step-11-testing)
  - [Push code from local](#push-code-from-local)
  - [View result from Jenkins UI](#view-result-from-jenkins-ui)

## Lab

- jenkins server: `172.28.40.221`
- gitlab server: `172.28.40.222`
- webapp server: `172.28.40.203`

## STEP 1 - Install Jenkins Server

### Jenkins

- ssh root@172.28.40.221
- useradd jenkins
- passwd jenkins
- id jenkins (check UID = 1000 => OK)
- su - jenkins
- sudo wget -O /etc/yum.repos.d/jenkins.repo <https://pkg.jenkins.io/redhat-stable/jenkins.repo>
- sudo rpm --import <https://pkg.jenkins.io/redhat-stable/jenkins.io.key>
- sudo yum install java-11-openjdk.x86_64 java-11-openjdk-devel.x86_64 -y
- sudo yum install jenkins -y
- sudo systemctl start jenkins
- sudo systemctl enable jenkins

### Nginx

- sudo yum install epel-release -y
- copy [nginx.repo](./files/nginx.repo) to /etc/yum.repos.d/
- sudo yum install nginx nginx-module* -y
- sudo mkdir -p /var/log/nginx/jenkins
- copy file [nginx.conf](./files/nginx.conf) to /etc/nginx/
- copy file [web.conf](./files/web.conf) to /etc/nginx/conf.d/
- sudo systemctl start nginx
- sudo systemctl enable nginx
- access browser: `172.28.40.221` and fill `password`

![screenshot](./images/01.png)

![screenshot](./images/02.png)

![screenshot](./images/03.png)

![screenshot](./images/04.png)

![screenshot](./images/05.png)

![screenshot](./images/06.png)

## STEP 2 - Install Gitlab Server

- ssh root@172.28.40.222
- curl -s <https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh> | sudo bash
- yum install gitlab-ce
- vim /etc/gitlab/gitlab.rb -> change `external_url`

![screenshot](./images/07.png)

- gitlab-ctl reconfigure
- access browser: `172.28.40.222` and enter `new password`

![screenshot](./images/08.png)

![screenshot](./images/09.png)

![screenshot](./images/10.png)

![screenshot](./images/11.png)

## STEP 3 - Config Webapp Server

- ssh root@172.28.40.203
- useradd dev
- passwd dev
- visudo -> add **`%dev    ALL=(ALL)       ALL`**
- su - dev
- sudo yum install java-11-openjdk.x86_64 java-11-openjdk-devel.x86_64 -y
- mkdir demo-01

## STEP 4 - Add Credentials for Webapp on Jenkins UI

- ssh jenkins@172.28.40.221
- ssh-keygen

![screenshot](./images/12.png)

- access: <http://172.28.40.221/credentials/store/system/domain/_/newCredentials>

### NOTE

- Username: `user 'dev' in webapp server`
- Private Key: `private key of user 'jenkins' in jenkins server`

![screenshot](./images/13.png)

![screenshot](./images/14.png)

## STEP 5 - Add Node on Jenkins UI

- ssh jenkins@172.28.40.221
- ssh-copy-id dev@172.28.40.203

![screenshot](./images/15.png)

- select `Manage Jenkins` -> `Manage Nodes and Clouds` -> `New Node` or access: <http://172.28.40.221/computer/new>

![screenshot](./images/16.png)

![screenshot](./images/17.png)

![screenshot](./images/18.png)

![screenshot](./images/19.png)

## STEP 6 - Setting gitlab plugin

### Install gitlab plugin on Jenkins UI

<http://172.28.40.221/pluginManager/available>

![screenshot](./images/20.png)

### Add a personal access token on Gitlab UI

<http://172.28.40.222/-/profile/personal_access_tokens>

![screenshot](./images/21.png)

![screenshot](./images/22.png)

### Add Credentials Gitlab token on Jenkins UI

<http://172.28.40.221/credentials/store/system/domain/_/newCredentials>

![screenshot](./images/23.png)

![screenshot](./images/24.png)

### Configure Gitlab on Jenkins UI

<http://172.28.40.221/configure>

![screenshot](./images/25.png)

## STEP 7 - Create new user, group, project on Gitlab UI

### Create new user

<http://172.28.40.222/admin/users/new>

![screenshot](./images/26.png)

![screenshot](./images/27.png)

![screenshot](./images/28.png)

![screenshot](./images/29.png)

![screenshot](./images/30.png)

### Create new group

<http://172.28.40.222/admin/groups/new>

![screenshot](./images/31.png)

![screenshot](./images/32.png)

![screenshot](./images/33.png)

### Create new project

<http://172.28.40.222/projects/new>

![screenshot](./images/34.png)

![screenshot](./images/35.png)

![screenshot](./images/36.png)

## STEP 8 - Create new Deploy Keys and enable in project on Gitlab UI

### Create deploy key

<http://172.28.40.222/admin/deploy_keys/new>

![screenshot](./images/37.png)

![screenshot](./images/38.png)

### Enable deploy key

<http://172.28.40.222/integration/demo-01/-/settings/repository>

![screenshot](./images/39.png)

![screenshot](./images/40.png)

![screenshot](./images/41.png)

## STEP 9 - Setting project on Gitlab UI

### Allow push for Developer

<http://172.28.40.222/integration/demo-01/-/settings/repository>

![screenshot](./images/42.png)

![screenshot](./images/43.png)

### Login user developer -> add ssh key -> clone source to local

![screenshot](./images/44.png)

![screenshot](./images/45.png)

![screenshot](./images/47.png)

![screenshot](./images/48.png)

## STEP 10 - Setting Pipeline, Webhook

### Create Pipeline on Jenkins UI

<http://172.28.40.221/view/all/newJob>

![screenshot](./images/49.png)

![screenshot](./images/50.png)

![screenshot](./images/51.png)

### Create Webhook on Gitlab UI (usage root user)

<http://172.28.40.222/admin/application_settings/network>

![screenshot](./images/52.png)

<http://172.28.40.222/integration/demo-01/hooks>

![screenshot](./images/53.png)

![screenshot](./images/54.png)

### Define pipeline script on Jenkins UI

<http://172.28.40.221/job/demo-01/configure>

**`reference`** [demo-01.groovy](./files/demo-01.groovy)

![screenshot](./images/55.png)

## STEP 11 - Testing

### Push code from local

![screenshot](./images/56.png)

### View result from Jenkins UI

![screenshot](./images/57.png)
