pipeline {
    agent {
        label 'dev-webapp'
    }

    environment {
        src = '/home/dev/demo-01'
    }

    stages {
        stage('checkout source code') {
            steps {
                dir("${src}") {
                    checkout([$class: 'GitSCM',
                        branches: [[name: '*/master']],
                        doGenerateSubmoduleConfigurations: false,
                        extensions: [],
                        submoduleCfg: [],
                        userRemoteConfigs: [[credentialsId: 'webapp-server',
                        url: 'git@172.28.40.222:integration/demo-01.git']]
                    ])
                }
            }
        }

        stage('clean tmp directory') {
            steps {
                dir("${src}@tmp") {
                    deleteDir()
                }
            }
        }
    }
}
